import { Component, OnInit } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

declare var Chart: any;
declare var $: any;

@Component({
	selector: 'app-resume',
	templateUrl: './resume.component.html',
	styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {

	public userToken;

	public data;
	public noData = true;

	public availableMonths;

	public selectedMonth;

	constructor(private http: Http) { }

	ngOnInit() {
		this.getObjects();
	}

	public getObjects(){
		this.userToken = "y0tc4e0vVkL8pACqIBazp8Uk7fnpnSoi";
		this.availableMonths = new Array();
		let d = new Date();
		
		for(var i = 0; i < 12; i++){
			this.availableMonths.push({
				value: i+1,
				name: this.getMonthString(i+1)+" "+d.getFullYear(),
				isSelected: (i == d.getMonth() ? true : null)
			});
		}

		this.selectedMonth = this.getMonthString(d.getMonth()+1)+" "+d.getFullYear();
		this.getMonthResume(d.getMonth()+1);
	}

	public getMonthResume(monthID){
		let d = new Date();
		this.selectedMonth = this.getMonthString(monthID)+" "+d.getFullYear();
		
		this._getBalanceFromMonth(monthID).then(
			value => {
				if(value.message == "Listed!"){
					this.noData = false;
					this.data = new Array();

					var totalExpenses = 0;
					var totalIncomes = 0;
					for(var i = 0; i < value.data.categories.length; i++){
						var category = value.data.categories[i];
						if(category.transaction_type == 1){
							this.data.push({
								name: category.name,
								percent: 42,
								value: category.value,
								color: category.color
							});
							totalExpenses += category.value;
						}else{
							this.data.push({
								name: category.name,
								percent: 42,
								value: category.value,
								color: category.color
							});
							totalIncomes += category.value;
						}
					}

					this.data[0].value = totalIncomes - totalExpenses;

					for(var i = 0; i < this.data.length; i++){
						var percentage = 100*(this.data[i].value/totalIncomes);
						this.data[i].percent = percentage;
					}

					if(!this.noData){
						this.doChart(this.data);
					}
				}else{
					this.noData = true;
				}
			}
		);
	}

	public doChart(chartData){
		let _labels = new Array();
		let _colors = new Array();
		let _data = new Array();

		_labels.push("");
		_data.push(0);
		_colors.push("#000");

		for(var i = 0; i < chartData.length; i++){
			var obj = chartData[i];

			_labels.push("");
			_data.push(obj.percent);
			_colors.push(obj.color);

			_labels.push("");
			_data.push(0);
			_colors.push("#000");
		}

		_labels.push("");
		_data.push(0);
		_colors.push("#000");


		$("#chartArea").replaceWith("<div class='col-xs-12' id='chartArea'><canvas id='bar-chart' width='100%' height='75'></canvas></div>");
		let ctx = $("#bar-chart")[0].getContext('2d');
		let chart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: _labels,
				datasets: [
				{
					label: "Value",
					backgroundColor: _colors,
					data: _data
				}
				]
			},
			options: {
				legend: { display: false },
				title: {
					display: false
				},
				scaleShowVerticalLines: false,
				scales: {
					xAxes: [{
						gridLines: {
							display : false
						}
					}],
					yAxes: [{
						type: 'linear',
						position: 'right',
						ticks: {
							beginAtZero: true,
							min: 0,
							max: 100,
							callback: function(value) {
								return value+"%";
							}
						}
					}]
				},
				tooltips: {
					callbacks: {
						title: function(tooltipItem, data) {
							return data['labels'][tooltipItem[0]['index']];
						},
						label: function(tooltipItem, data) {
							return parseFloat(data['datasets'][0]['data'][tooltipItem['index']]).toFixed(2)+"%";
						}
					},
					backgroundColor: 'rgba(0, 0, 0, 0.78)',
					titleFontSize: 16,
					titleFontColor: '#fff',
					bodyFontColor: '#fff',
					bodyFontSize: 16,
					displayColors: false
				}
			}
		});
	}

	_getBalanceFromMonth(monthID): Promise<any>{
		let d = new Date();
		let URL = "http://ec.piercontent.com/api/v1/users/1/balance?api_token="+this.userToken+"&date="+d.getFullYear()+","+monthID;
		return this.http.get(URL)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	_deleteTransaction(transactionID): Promise<any>{
		let URL = "http://ec.piercontent.com/api/v1/users/1/transactions/"+transactionID+"?api_token="+this.userToken;
		return this.http.delete(URL)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	private extractData(res: any) {
		return res.json();
	}

	private handleError(error: any){
		if (error.status == 404) {
		    return {message:"null"};
		}else{
			console.error(error);
			alert("Ocorreu um erro, por favor, atualize a página.");
		}
	}

	public getMonthString(month){
		switch (month) {
			case 1:
				return "Janeiro";
			case 2:
				return "Fevereiro";
			case 3:
				return "Março";
			case 4:
				return "Abril";
			case 5:
				return "Maio";
			case 6:
				return "Junho";
			case 7:
				return "Julho";
			case 8:
				return "Agosto";
			case 9:
				return "Setembro";
			case 10:
				return "Outubro";
			case 11:
				return "Novembro";
			case 12:
				return "Dezembro";
		}
	}

}
