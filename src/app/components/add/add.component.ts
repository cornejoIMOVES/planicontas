import { Component, OnInit } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';

declare var $: any;
declare var Date: any;

@Component({
	selector: 'app-add',
	templateUrl: './add.component.html',
	styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

	public userToken;
	public isLoaded = false;

	public allCategories;
	public categorias;

	public actualDate;
	public actualDateCustom;
	public isNew;
	public isSelected;
	public selectedCategory;

	public tipo;

	public categoryColors = ["#fee633","#881edd","#fd7022","#00ceff","#4577ea","#fd2145","#00edc9","#f432a4"];
	public selectedCategoryColor = "green";

	constructor(private http: Http, private router: Router) { }

	ngOnInit() {
		this.getObjects();
		this.setProperties();
	}

	public getObjects(){
		this.userToken = "y0tc4e0vVkL8pACqIBazp8Uk7fnpnSoi";
		this.tipo = "Selecione";

		this._getAllCategories().then(
			value => {
				this.allCategories = value;
				this.isLoaded = true;
			}
		);
	}

	public setProperties(){
		this.actualDate = this.getActualDate(2);
		this.actualDateCustom = this.getCustomFormattedDate(this.actualDate);
	}

	public setCategories(categoryType){
		$("#dropdownMenu1").html("Selecione");
		this.tipo = categoryType == 1 ? "Despesa" : "Receita";
		this.isNew = false;
		this.isSelected = false;

		if(categoryType == 1){
			this.categorias = [];
			for(var i = 0; i < this.allCategories.data.length; i++){
				var category = this.allCategories.data[i];
				if(category["transaction_type_id"] == 1){
					this.categorias.push({
						id: category["id"],
						color: category["color"],
						name: category["name"],
						type: 1
					});
				}
			}
			this.categorias.push({
				id: 3301,
				name: "Criar nova categoria",
				type: 1
			});
		}else{
			this.categorias = [];
			for(var i = 0; i < this.allCategories.data.length; i++){
				var category = this.allCategories.data[i];
				if(category["transaction_type_id"] == 2){
					this.categorias.push({
						id: category["id"],
						color: category["color"],
						name: category["name"],
						type: 2
					});
				}
			}
			this.categorias.push({
				id: 3301,
				name: "Criar nova categoria",
				type: 2
			});
		}
	}

	public display(category){
		if(category.id == 3301){
			$("#dropdownMenu1").html("<span style='font-size: 21px;color: #545454;padding-left: 15px;line-height: 45px;'>"+category.name+"</span>");
			this.isNew = true;
			this.isSelected = true;
			this.selectedCategory = category;
		}else{
			$("#dropdownMenu1").html("<i class='fa fa-circle item-circle select-li-i' aria-hidden='true' style='color: "+category.color+";'></i><span style='font-size: 21px;color: #545454;padding-left: 15px;line-height: 45px;'>"+category.name+"</span>");
			this.isNew = false;
			this.isSelected = true;
			this.selectedCategory = category;
		}
	}

	public chooseColor(){
		$("#prompt").show();
	}

	public selectColor(id){
		this.selectedCategoryColor = this.categoryColors[id-1];
		this.closePrompt();
	}

	public closePrompt(){
		$("#prompt").hide();
	}

	public showDatePicker(){
		$("#datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
		$("#datepicker").datepicker("show");
	}

	public addTransaction(){
		if(this.selectedCategory.id != 3301){
			this.actualDate = this.getOriginalDateFormat($("#datepicker").val());
			var params = {
				"category_id": this.selectedCategory.id,
				"transaction_type_id": this.selectedCategory.type,
				"name": this.selectedCategory.name,
				"value": $("#value").val(),
				"date": this.actualDate,
				"created_at": this.getActualDate(1)
			};

			this._createTransaction(params).then(
				value => {
					if(value.message == "Created!"){
						this.router.navigate(['/']);
					}else{
						console.log(value);
					}
				}
			);
		}else{
			var newCategoryParams = {
				"transaction_type_id": this.selectedCategory.type,
				"name": $("#name").val(),
				"color": this.selectedCategoryColor
			};

			this._createCategory(newCategoryParams).then(
				value => {
					if(value.message == "Created!"){
						this.selectedCategory = {
							id: value.data.id,
							color: value.data.color,
							name: value.data.name,
							type: value.data.transaction_type_id
						};
						this.addTransaction();
					}else{
						console.log(value);
					}
				}
			);
		}
	}



	_getAllCategories(): Promise<any>{
		let URL = "http://ec.piercontent.com/api/v1/users/1/categories?api_token="+this.userToken;
		return this.http.get(URL)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	_createCategory(params): Promise<any>{
		let URL = "http://ec.piercontent.com/api/v1/users/1/categories?api_token="+this.userToken;
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		return this.http.post(URL, params, options)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	_createTransaction(params): Promise<any>{
		let URL = "http://ec.piercontent.com/api/v1/users/1/transactions?api_token="+this.userToken;
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		return this.http.post(URL, params, options)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	private extractData(res: any) {
		return res.json();
	}

	private handleError(error: any){
		if (error.status == 404) {
		    return {message:"null"};
		}else{
			console.error(error);
			alert("Ocorreu um erro, por favor, atualize a página.");
		}
	}

	public getActualDate(dateType: any): String{
		var d = new Date();
		if(dateType == 1){
			var month = d.getMonth()+1;
			var seconds = d.getSeconds();
			if(month < 10){
				month = "0"+month;
			}
			if(seconds < 9){
				seconds = "0"+seconds;
			}
			return d.getFullYear()+"-"+(month)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+seconds;
		}else{
			var month = d.getMonth()+1;
			if(month < 10){
				month = "0"+month;
			}
			return d.getFullYear()+"-"+(month)+"-"+d.getDate();
		}
	}

	public getCustomFormattedDate(str: any): String{
		var r = str.split("-");
		return r[2]+"/"+r[1]+"/"+r[0];
	}

	public getOriginalDateFormat(str: any): String{
		var r = str.split("/");
		return r[2]+"-"+r[1]+"-"+r[0];
	}
}
