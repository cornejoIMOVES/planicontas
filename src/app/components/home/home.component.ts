import { Component, OnInit } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

declare var Chart: any;
declare var $: any;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	public userToken;

	public data;

	public _balance;
	public _date;

	public isNull = true;

	constructor(private http: Http) { }

	ngOnInit() {
		this.getObjects();
	}

	public getObjects(){
		this.userToken = "y0tc4e0vVkL8pACqIBazp8Uk7fnpnSoi";
		let d = new Date();

		this._getBalance().then(
			value => {
				if(value.message == "Listed!"){
					this.isNull = false;
					this.data = new Array();
					this._balance = "R$ "+value.data.overall.value+",00";
					this._date = this.getMonthString(value.data.overall.month)+" "+d.getFullYear();

					var totalExpenses = 0;
					var totalIncomes = 0;
					for(var i = 0; i < value.data.categories.length; i++){
						var category = value.data.categories[i];
						if(category.transaction_type == 1){
							this.data.push({
								name: category.name,
								percent: category.value,
								value: (category.value * -1),
								color: category.color
							});
							totalExpenses += category.value;
						}else{
							this.data.push({
								name: category.name,
								percent: 3301,
								value: category.value,
								color: "rgb(0, 220, 0)"
							});
							totalIncomes += category.value;
						}
					}

					if(totalIncomes > 0){
						var finalTotal = totalIncomes - totalExpenses;
						this.data[0].percent = finalTotal;
					}

					this.processData();
				}else{
					this.isNull = true;
					this._balance = "R$ 0,00";
					this._date = this.getMonthString(d.getMonth()+1)+" "+d.getFullYear();

					this.data = new Array();
					this.data.push({
						name: "Sem lançamentos",
						percent: 1,
						value: 0,
						color: "#E9E9E9"
					});

					this.processData();
				}
			}
		);
	}

	public processData(){
		this.doChart(this.data);
	}

	public doChart(chartData){
		let _data = new Array();
		let _colors = new Array();
		let _labels = new Array();

		for(var i = 0; i < chartData.length; i++){
			var obj = chartData[i];

			_data.push(obj.percent);
			_colors.push(obj.color);
			_labels.push(obj.name);
		}

		let data = {
			datasets: [{
				data: _data,
				backgroundColor: _colors
			}],
			labels: _labels
		};

		let options = {        
			cutoutPercentage: 88,
			legend: {
				display: false
			},
			tooltips: {
				callbacks: {
					title: function(tooltipItem, data) {
						return data['labels'][tooltipItem[0]['index']];
					},
					label: function(tooltipItem, data) {
						return "R$ "+data['datasets'][0]['data'][tooltipItem['index']]+",00";
					}
				},
				backgroundColor: 'rgba(0, 0, 0, 0.78)',
				titleFontSize: 16,
				titleFontColor: '#fff',
				bodyFontColor: '#fff',
				bodyFontSize: 16,
				displayColors: false
			}
		};

		let ctx = $("#chart")[0].getContext('2d');
		let chart = new Chart(ctx, {
			type: 'doughnut',
			data: data,
			options: options
		});
	}

	_getBalance(): Promise<any>{
		let d = new Date();
		let URL = "http://ec.piercontent.com/api/v1/users/1/balance?api_token="+this.userToken+"&date="+d.getFullYear()+","+(d.getMonth()+1);
		return this.http.get(URL)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	private extractData(res: any) {
		return res.json();
	}

	private handleError(error: any){
		if (error.status == 404) {
		    return {message:"null"};
		}else{
			console.error(error);
			alert("Ocorreu um erro, por favor, atualize a página.");
		}
	}

	public getMonthString(month){
		switch (month) {
			case 1:
				return "Janeiro";
			case 2:
				return "Fevereiro";
			case 3:
				return "Março";
			case 4:
				return "Abril";
			case 5:
				return "Maio";
			case 6:
				return "Junho";
			case 7:
				return "Julho";
			case 8:
				return "Agosto";
			case 9:
				return "Setembro";
			case 10:
				return "Outubro";
			case 11:
				return "Novembro";
			case 12:
				return "Dezembro";
		}
	}
}
