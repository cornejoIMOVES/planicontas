import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

	public logoOi = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABNCAYAAAAW92IAAAAGyUlEQVR4nOXceaxcdRXA8c8b2kppU6DS0opowUZrCEGLRE1cAGMkqRq1VBP3JRaXRCHELRpBY8S4xS0RNcYFUVyqCW4xbRQqDSAxVrBKWqQgRR5WtKU7fc+ff5w37bz77p25d+a+mTftN5nMm9+ce+75nffbz7kzklLSJUvxXKyYeD0Ji7EIJ+DUCbnHsA97sAs78SB2YDu2YSse6taQXhip4IAGLsSrcAmeUrMtD+NP+CPuwE3YXfM9plDGAfPxDrwLZ023QS2MC0dswE+Fc2qnnQMauAxXi6Y9aLbiB/iO6Dq1UOSAJ+I60eRnGuO4EV/Ezb0qy3PAM/ELPKFX5X1gE96HW7tVkHXAudiIU3qzq+/8DJfjH1UvbHXAQmzGmfXZ1VcexfvxdVSa2pp8zfBWHhbgWjE+lG7BzRbwQjHvHitsxSvwt06CzRbw4Wk1p/88Fbfg/E6CIymlZbgXI9Ns1CDYjZfg9iKBBlY5NisPJ4spfXmRQEP0/2OZ0/Bz4YwpNPD0vpozGFbgG3lfNMSy93hgDV6dLRxJKY2J/fvxwL/FeHBkm91QYdV0DHAaPtBaMJJS2jnxxfHCfnGYM0q0gH8O1Jz+cxLWNj80cPfgbBkYazGLcMC0HDXNcM4Q55oaYs18PLKaGARn41+G7xCkV0axtIHDYql4vLEEy5vb4esHaUkHdmAdfq/+NcsFTQes18V5Wh/4ppizL8ULcLGYx+tiZdMB/8OXa1RcBw/h3SK01uQmXFPjPc7Kngn+p0blvXIHDuWU31bjPZa1OmAPPl+j8l5ZVrG8GxZn4wILRNhpYY036YW34lstnx+PP+DsmvTvzosMfVC9/axXbsBvRXzyMvUe3R/Kc8B8cUi6qMYbzVQONXIK95qeseCgcOz9YvFVhV0iKFo3e/McQERYdtVxA3wWKzFPzOnLJv6+EN/GWAcdN4jW+ByRaVIne6SUil6fTL2xKaV0Rhv9zdd5KaXtBTr+m1Ja0CL7kR5tynJnO8MWp5T2d6l4fUppbhvd2deSlNLWHD1fyMgtSimNd2lTHhuKugCxQ/xRF81qFK/DgYrXvNLkVR/8KvN5J+7qwqYi7mvnACILoyqfEM5r5SQxFmzDnbjS5Mg0bBFZKU2S/MSHTV3YVMQ9ZZrnzRWa1J6U3/R/nSP76Ry5c1u+315gz1sq2NOJl3ZqAcSOrCwbTW36LzZx/JThClODMneJHEKKd6d17lo3l3HAOrFPKENe/3xZgewsEbnNsmXiveie2e7VLaPYUcYB+/CTkkofzil7chv5vLzDpo6iJMmqi6gibmHqQFTEupJyJ+aUHWwjnzdTzJ14n11wTVF5VX5HeQdsUK4bLM0p+3Mb+c05ZUsm3ucVXFOXA35DeQccEsdmnXheTtn18g82HsjReaKjaS1Fm7E68hfvxt8p7wDKZWWeZ2q/vh/vFcduTfbjzaYufC5xtAsUJWPXkaR945G/CubavNczSs6t1xZcf35K6eqU0odSSmcXyNya0XVmjswPK8/2U1nZ1FfFAbNTSodLKB9LKV1cQW/z9fYcXW/MkdtRsbJZ/tqqr0oXOIx7SsidILawF1TQvQZfySlfnfm8UsT1emFyqkzF/1LekraIvSmlK1NKc9roOyWl9LlUvMMbT7Fdbsr/uML989ifUlrYasOsit57tILsPLEBukIkM98mlrlzxEh+EV6ufUyyIR6WuBzPFwGSXvi+zNF/lUdm4Lt4Q49GDIoxPE0cyx2hyhhA/kpvWLhOpvJUd8Dp9djSdw7g43lfVHXAsCZVXoP78r6oMgasUCL9fAZyL85RsCmr0gJW1WJOfxkX4bXCHWlZB4xoSS0bIj6jwx6mbBdYo7sT4kFyu0iqyG64JlHGAfPwF/WGpaebB0UkaUcnwTJd4EuGq/IHxB6iY+Xp7IB3ikFkWBjDa7V5RCZLuy5wqdjVDUsq/TheL2wuTVELeJN4UHlYKj8mWmqlyiN3N7hWhMeH5UGqfXgNftnNxdkusEpkjQ5L5UfFA5Kl+3yWVgcsEZGdYXl4YqP4z4/2oqR1DPio4aj8OD6FF+mx8hxtAQ08YuZnjG/B2/TQ5LM0W8BiM7vye3GVOBStrfIcbQFzRFLU3PbifWdMJEpeZZp+ZqfZAh4zOTtj0BwUP4RwjpiWp+03hlpngfkiZHTRdN2sBNvFr8R8VX15AG3JrgMeh4/hPfrXHUaF478nYvZ9fZCzaC9wulgOr8azVD87bMdukfC8XoTdNxvg06tlzgNOxrNF5He5yPhYKrrMqS0yTSftFqP2IyKf5wGxNd0qFlrbTI4UD5T/A32YxTaHVDEmAAAAAElFTkSuQmCC";

	public data;

	constructor() { }

	ngOnInit() {
		this.getData();
	}

	public getData(){
		this.data = [
			{
				title: "Você quer ganhar dinheiro ou acumular riqueza?",
				content: "Voce sabe a diferenca entre ganhar dinheiro e acumular riqueza? Conhecendo..."
			},
			{
				title: "Você sabe qual é a relacao entre a taxa Selic e os precos do Tesouro Direto?",
				content: "Reducao na taxa basica de juros da economia nem sempre r..."
			},
			{
				title: "Entao voce e um empreendedor!",
				content: "Voce teve uma idea na quinta, validou na sexta, no sabado montou o produto..."
			}
		];
	}
}
