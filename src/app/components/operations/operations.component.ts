import { Component, OnInit } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Component({
	selector: 'app-operations',
	templateUrl: './operations.component.html',
	styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {

	public userToken;

	public data;
	public noData = true;

	public _date;

	constructor(private http: Http) { }

	ngOnInit() {
		this.getObjects();
	}

	public getObjects(){
		this.userToken = "y0tc4e0vVkL8pACqIBazp8Uk7fnpnSoi";
		let d = new Date();

		this._getTransactions().then(
			value => {
				if(value.message == "Listed!"){
					this.noData = false;
					this.data = new Array();
					this._date = this.getMonthString(d.getMonth()+1)+" "+d.getFullYear();

					for(var categoryID in value.data){
						var category = value.data[categoryID];
						if(category.transaction_type_id == 1){
							this.data.push({
								id: category.id,
								name: category.name,
								value: "- "+category.value+",00",
								color: category.color
							});
						}else{
							this.data.push({
								id: category.id,
								name: category.name,
								value: "+ "+category.value+",00",
								color: category.color
							});
						}
					}
				}else{
					this.noData = true;
				}
			}
		);
	}

	public deleteTransaction(transactionID){
		this._deleteTransaction(transactionID).then(
			value => {
				if(value.message == "Transaction successfully deleted"){
					this.getObjects();
				}
			}
		);
	}

	_getTransactions(): Promise<any>{
		let d = new Date();
		let URL = "http://ec.piercontent.com/api/v1/users/1/transactions?api_token="+this.userToken+"&date="+d.getFullYear()+","+(d.getMonth()+1);
		return this.http.get(URL)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	_deleteTransaction(transactionID): Promise<any>{
		let URL = "http://ec.piercontent.com/api/v1/users/1/transactions/"+transactionID+"?api_token="+this.userToken;
		return this.http.delete(URL)
	  		.toPromise()
	  		.then(this.extractData)
	  		.catch(this.handleError);
	}

	private extractData(res: any) {
		return res.json();
	}

	private handleError(error: any){
		if (error.status == 404) {
		    return {message:"null"};
		}else{
			console.error(error);
			alert("Ocorreu um erro, por favor, atualize a página.");
		}
	}

	public getMonthString(month){
		switch (month) {
			case 1:
				return "Janeiro";
			case 2:
				return "Fevereiro";
			case 3:
				return "Março";
			case 4:
				return "Abril";
			case 5:
				return "Maio";
			case 6:
				return "Junho";
			case 7:
				return "Julho";
			case 8:
				return "Agosto";
			case 9:
				return "Setembro";
			case 10:
				return "Outubro";
			case 11:
				return "Novembro";
			case 12:
				return "Dezembro";
		}
	}
}
