import { Component } from '@angular/core';
import { Router, NavigationEnd, RoutesRecognized } from '@angular/router'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [
	'./app.component.css'
	]
})
export class AppComponent {
	
	public showDrawer;

	constructor(private _r: Router) {
	    this._r.events.subscribe(event => {
	    	if (event instanceof RoutesRecognized) {
	    		if(event.url == '/inscricao'){
	    			this.showDrawer = false;
	    		}else{
	    			this.showDrawer = true;
	    		}
	    	}
	    });
	}
}
