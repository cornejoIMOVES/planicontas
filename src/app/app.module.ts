import { BrowserModule }          from '@angular/platform-browser';
import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { HttpModule }             from '@angular/http';
import { FormsModule }            from '@angular/forms';

import { AppComponent }           from './app.component';
import { RegistryComponent }      from './components/registry/registry.component';
import { HomeComponent }          from './components/home/home.component';
import { OperationsComponent }    from './components/operations/operations.component';
import { ResumeComponent }        from './components/resume/resume.component';
import { AddComponent }           from './components/add/add.component';
import { BlogComponent }          from './components/blog/blog.component';
import { DrawerComponent }        from './components/drawer/drawer.component';

const appRoutes: Routes = [
  { 
    path: 'inscricao', 
    component:  RegistryComponent
  },
  { 
    path: '', 
    component: HomeComponent 
  },
  { 
    path: 'receita_e_despesas', 
    component:  OperationsComponent
  },
  { 
    path: 'relatorio', 
    component:  ResumeComponent
  },
  { 
    path: 'adicionar', 
    component:  AddComponent
  },
  { 
    path: 'blog', 
    component:  BlogComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    RegistryComponent,
    HomeComponent,
    OperationsComponent,
    ResumeComponent,
    AddComponent,
    BlogComponent,
    DrawerComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false 
      }
    ),
    HttpModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
